﻿<%@ Page Language="C#" Inherits="Assignment1_A.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
<meta charset="utf-8" />
    <title>Assignment 1</title>    
    <link href="styles.css" rel="stylesheet" />
</head>
<body>
    <!--REQUIRED VALIDATION IS IN EVERY INPUT-->
    <!--REGULAR EXPRESSION IS IN THE POSTAL CODE-->
    <!--COMPARE VALIDATION IS IN PHONE NUMBER-->
    <!--VALIDATION SUMMARY SHOW ON TOP-->
    <h1>Secure Checkout</h1>
    <form id="form1" runat="server">   
        <div id="container">
            <section id="mailInfo">
                <table>
                    <tr>
                        <td colspan="2">
                            <p class="mailInfo"><b>SHIPPING INFORMATION</b></p>   
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:validationsummary forecolor="Red" runat="server" id="validationSummary"></asp:validationsummary>
                        </td>            
                   </tr>
                    <tr>
                        <td>
                            <b>FIRST NAME</b>
                    
                        </td>
                        <td>
                            <b class="space">LAST NAME</b>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox runat="server" ID="clientFName" placeholder="e.g John" Width="240" ></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Required first name" ControlToValidate="clientFName" ID="validatorfname" ForeColor="Red"></asp:RequiredFieldValidator>
                    
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="clientLName" placeholder="e.g Smith" Width="240"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Required last name" ControlToValidate="clientLName" ID="validatorlname" ForeColor="Red"></asp:RequiredFieldValidator>
                        </td>
                       
                    </tr>
                <tr>
                    <td>
                        <b>ADDRESS</b>
                    </td>           
                </tr>
                <tr>
                     <td>
                        <asp:TextBox runat="server" ID="address" placeholder="e.g 123 Around The Corner Road"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Required address" ControlToValidate="address" ID="validatorAddress" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>TOWN/CITY</b>
                    </td>               
                </tr>
                <tr>
                    <td>
                        <asp:TextBox runat="server" ID="city" placeholder="e.g Toronto"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Required city" ControlToValidate="city" ID="validatorCity" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>PROVINCE</b>
                    </td>               
                </tr>   
                <tr>
                    <td>
                        <asp:DropDownList ID="provinces" runat="server"  AppendDataBoundItems="True" CssClass="ddlstyle">
                            <asp:ListItem Enabled="true" Text="Select Province" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="Alberta" Value="1"></asp:ListItem>
                            <asp:ListItem Text="British Columbia" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Manitoba" Value="12"></asp:ListItem>
                            <asp:ListItem Text="New Brunswick" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Newfoundland and Labrador" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Northwest Territories" Value="12"></asp:ListItem>
                            <asp:ListItem Text="Nova Scotia" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Nunavut" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Ontario" Value="12"></asp:ListItem>
                            <asp:ListItem Text="Prince Edward Island" Value="12"></asp:ListItem>
                            <asp:ListItem Text="Quebec" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Saskatchewan" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Yukon" Value="12"></asp:ListItem>
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator ID="req_prov" runat="server" ControlToValidate="provinces" ErrorMessage="Required province" InitialValue="Select Province"></asp:RequiredFieldValidator>

                    </td>
                </tr>
                <tr>
                    <td>
                        <b>POSTAL CODE</b>
                    </td>             
                </tr>    
                <tr>
                     <td class="auto-style2">
                        <asp:TextBox runat="server" ID="postcode" placeholder="e.g M6M 1J6"></asp:TextBox> 
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Required Postal Code" ControlToValidate="postcode" ID="RequiredFieldValidator11" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator Display = "Dynamic" ControlToValidate = "postcode" ID="RegularExpressionValidator1" ValidationExpression = "(\D{1}\d{1}\D{1}\-?\d{1}\D{1}\d{1})|(\D{1}\d{1}\D{1}\ ?\d{1}\D{1}\d{1})" 
                                                        runat="server" ErrorMessage="Please Enter Valid Postal Code" ForeColor="Red"></asp:RegularExpressionValidator>
                
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>EMAIL</b>
                    
                    </td>
                    <td>
                        <b>CONFIRM EMAIL</b>
                    </td>                
                </tr>
                <tr>
                    <td>
                        <asp:TextBox runat="server" ID="email" placeholder="e.g sample@gmail.com" Width="240" ></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Required email" ControlToValidate="email" ID="RequiredFieldValidator1" ForeColor="Red"></asp:RequiredFieldValidator>
                    
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="confirm_email" placeholder="" Width="240"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Confirm email" ControlToValidate="confirm_email" ID="RequiredFieldValidator2" ForeColor="Red"></asp:RequiredFieldValidator>
                        <asp:CompareValidator runat="server" id="cmpEmail" controltovalidate="email" controltocompare="confirm_email" operator="Equal" type="String" 
                                              errormessage="Email is not the same" Display="Dynamic"/><br />
                    </td>                       
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox runat="server" ID="phone" placeholder="e.g 416-689-1234" Width="240" ></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Required phone number" ControlToValidate="phone" ID="RequiredFieldValidator112" ForeColor="Red"></asp:RequiredFieldValidator>                   
                    </td>  
                </tr>
        </table>
        </section>
          
        <section id="cart">           
            <table class="cart">
                <tr>
                    <td colspan="3">
                         <p><b>ORDER SUMMARY</b></p> 
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Image ID="Image3" runat="server" ImageUrl="~/images/img3.png"/>
                    <td>
                        <b class="space">Raspberry Pi 3<br />Qty.1</b>
                    </td>
                    <td>
                        $109.99<br />CAD
                    </td>
                </tr>               
                 <tr>
                    <td>
                        <asp:Image ID="Image4" runat="server" ImageUrl="~/images/img4.png"/>
                    </td>
                    <td>
                        <b class="space">Arduino Uno<br />Qty. 1</b>
                    </td>
                    <td>
                        $99.99<br />CAD
                    </td>               
                </tr>   
                <tr>
                    <td colspan="3">                      
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td>
                        <p class="sub"><b>Subtotal</b> <br /><b>Shipping</b><br /><b>Taxes</b></p>
                    </td>
                    <td>
                   
                    </td>
                    <td>
                        <p class="left">$209.98<br />---------- <br /> $27.29</p>
                    </td>
                </tr>             
                <tr>
                    <td colspan="3">
                         <hr />
                    </td>
                </tr>
                <tr>
                    <td>                    
                        <p class="sub"><b>Order Total</b></p>
                    </td>
                    <td>
                   
                    </td>
                    <td>
                        <p class="left">$237.27</p>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                        <p class="dd"><b>Delivery Option</b>  </p>
                    </td>                  
                </tr>
                <tr>
                     <td colspan="2">
                        <asp:RadioButton id="RadioButton1" runat="server" GroupName="payment" Text="Standard (Free)"></asp:RadioButton>                      
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RadioButton id="RadioButton2" runat="server" GroupName="payment" Text="Express 2-5 days ($9.99)"></asp:RadioButton>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <hr />
                        <asp:CheckBox runat="server" ID="tandc" Text="I have read and accept the terms and conditions" />                       
                    </td>                   
                </tr>
                <tr>
                    <td colspan ="3">
                        <asp:CheckBox runat="server" ID="newsletter" Text="Subcribe for our email newsletter?"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:Button class="button b2" Text="PROCEED TO CHECKOUT" runat="server" />
                    </td>
                </tr>
             </table>        
            
            
        </section>
        <section id="billInfo">
            <table>
                <tr>
                    <td colspan="2">
                        <p class="mailInfo"><b>BILLING INFORMATION</b></p>   
                        <hr/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:RadioButton id="cc" runat="server" GroupName="payment" Text="CREDIT CARD"></asp:RadioButton>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/img.png"/>
                    </td>
                    <td>
                        <asp:RadioButton id="paypay" runat="server" GroupName="payment" Text="Paypal"></asp:RadioButton>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/img2.png"/>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <b>NAME (as it appears on your CC)</b>         
                    </td>                           
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:TextBox runat="server" ID="cc_name" placeholder="e.g John Smith"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ErrorMessage="Required name" ControlToValidate="cc_name" ID="RequiredFieldValidator3" ForeColor="Red"></asp:RequiredFieldValidator>                 
                    </td>                
                </tr>
            <tr>
                <td class="auto-style1">
                    <b>CARD NUMBER</b>
                </td>           
            </tr>
            <tr>
                    <td>
                    <asp:TextBox runat="server" ID="cc_number" placeholder="0000-0000-0000-0000"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Required credit card number" ControlToValidate="cc_number" ID="RequiredFieldValidator5" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>         
            <tr>
                <td>
                    <b>EXPIRATION DATE</b>                   
                </td>
                <td>
                    <b>CVV</b>
                </td>                
            </tr>
            <tr>
                <td>
                    <asp:TextBox runat="server" ID="cc_exp" placeholder="MM/YY" Width="240" ></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Required CC expiration" ControlToValidate="clientFName" ID="RequiredFieldValidator8" ForeColor="Red"></asp:RequiredFieldValidator>
                    
                </td>
                <td>
                    <asp:TextBox runat="server" ID="cc_cvv" placeholder="" Width="240"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Required CVV" ControlToValidate="clientLName" ID="RequiredFieldValidator9" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>                      
            </tr>
            <tr>
                <td>
                    <b>BILLING ADDRESS</b>
                </td>
            </tr>
            <tr>
                <td>
                     <asp:CheckBox runat="server" ID="CheckBox5" Text="Same as mailing address" />
                </td>
            </tr>
    </table>
    </section>
    </div>
    </form>
</body>
</html>
</html>